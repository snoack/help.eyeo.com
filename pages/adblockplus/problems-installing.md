title=Problems installing
description=Problems installing Adblock Plus on your desktop browser? Learn more here.
template=article
product_id=abp
category=Installation

Refer to the suggestions below if you are having trouble installing Adblock Plus.  

<section class="platform-chrome" markdown="1">
## Chrome

1. Ensure that you downloaded the extension from [AdblockPlus.org](https://adblockplus.org) OR the [Chrome Web Store](https://chrome.google.com/webstore/detail/adblock-plus/cfhdojbkjhnklbpkdaibdccddilifddb).
2. Ensure that your browser is on version 41 or higher. Follow the steps below to check your browser version.
    1. Open Chrome.
    2. Click the **Chrome menu** icon, hover over **Help** and select **About Google Chrome**.
    <br>The version is displayed below the Chrome icon. If your browser is out-of-date, Chrome will automatically start the update process.
3. [Uninstall](adblockplus/uninstall-adblock-plus) and reinstall Adblock Plus.
4. If you are still experiencing issues, try [restoring Chrome settings to default](https://support.google.com/chrome/answer/3296214).
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. Ensure that you downloaded the extension from [AdblockPlus.org](https://adblockplus.org) OR [Firefox Add-ons](https://addons.mozilla.org/en-US/firefox/addon/adblock-plus/?src=ss).
2. Ensure that your browser is on version 51 or higher. Follow the steps below to check your browser version.
    1. Open Firefox.
    2. **Windows**: Click the **Firefox menu** icon, select **Help** and then select **About Firefox**.
    <br>**Mac**: From the Mac menu bar, click **Firefox** and select **About Firefox**.
    <br>The *About Firefox* window opens.
    <br>The version is displayed next to the Firefox icon. If your browser is out-of-date, Firefox will automatically start the update process.
3. [Uninstall](adblockplus/uninstall-adblock-plus) and reinstall Adblock Plus.
4. Receiving a 'corrupt download' or 'conflict with other extensions' error? Refer to this Mozilla article about the [inability to install add-ons](https://support.mozilla.org/en-US/kb/unable-install-add-ons-extensions-or-themes?redirectlocale=en-US&redirectslug=unable-install-add-ons-or-extensions).
5. If you are still experiencing issues, try [refreshing Firefox](https://support.mozilla.org/en-US/kb/refresh-firefox-reset-add-ons-and-settings).
</section>

<section class="platform-msie" markdown="1">
## Internet Explorer

1. Ensure that you downloaded the extension from [AdblockPlus.org](https://adblockplus.org).
2. Ensure that your browser is on version 8 or higher. Follow the steps below to check your browser version.
    1. Open Internet Explorer.
    2. Click the **Help** icon.
    3. Select **About Internet Explorer**.
    <br>The *About Internet Explorer* window opens.
    <br>The version is displayed below the Internet Explorer icon.
3. [Uninstall](adblockplus/uninstall-adblock-pluss)and reinstall Adblock Plus.
</section>

<section class="platform-opera" markdown="1">
## Opera

1. Ensure that you downloaded the extension from [Opera Add-ons](https://addons.opera.com/en/extensions/details/opera-adblock/?display=en-US).
2. Ensure that your browser is on version 28 or higher. Follow the steps below to check your browser version.
    1. Open Opera.
    2. **Windows**: Click the **Opera menu** icon and select **About Opera**.
    <br>**Mac**: From the Mac menu bar, click **Opera** and select **About Opera**.
    <br>The *About Opera* tab opens.
    <br>The version is displayed below *Version information*.
    <br>If your browser is out-of-date, Opera will automatically start the update process.
3. [Uninstall](adblockplus/uninstall-adblock-plus) and reinstall Adblock Plus.
</section>

<section class="platform-safari" markdown="1">
## Safari

1. Ensure that you downloaded the extension from [AdblockPlus.org](https://adblockplus.org).
2. Ensure that your browser is on version 6.0 or higher. Follow the steps below to check your browser version.
    1. Open Safari.
    2. From the Mac menu bar, click **Safari** and select **About Safari**.
    <br>The *About Safari* window opens.
    <br>The version is displayed below the Safari icon.
3. [Uninstall](adblockplus/uninstall-adblock-plus) and reinstall Adblock Plus.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. Ensure that you downloaded the extension from [AdblockPlus.org](https://adblockplus.org).
2. Ensure that your browser is on version 16.6 or higher. Follow the steps below to check your browser version.
    1. Open Yandex Browser.
    2. Click the **Yandex Browser menu** icon, hover over **Advanced** and select **About Yandex Browser**.
    <br>The *About* tab opens.
    <br>The version is displayed to the left of the Yandex.Browser icon.
3. [Uninstall](adblockplus/uninstall-adblock-plus) and reinstall Adblock Plus.

If you still cannot install Adblock Plus after trying these suggestions, please contact <support@adblockplus.org>.
</section>
