title=Block a specific element
description=Block almost any element on a webpage, like images or comment sections.
template=article
product_id=abp
category=Customization & Settings

You can block more than just ads with Adblock Plus. Block images, fonts, scripts, comment sections, and more by following the steps below.

<section class="platform-chrome" markdown="1">
## Chrome

1. Open the webpage with the element you want to block.
2. From the Chrome toolbar, click the **Adblock Plus** icon and select **Block element**.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
3. Hover your mouse over the element you want to block. 
<br>The element area turns yellow.
4. Double-click the element.
<br>The *Block element* window opens.
5. Click **Add**.
<br>The element is now blocked.
</section>

[Learn how to view a blocked item or unblock an element](adblockplus/view-blocked-items)

<section class="platform-firefox" markdown="1">
## Firefox

1. Open the webpage with the element you want to block.
2. From the Firefox toolbar, click the **Adblock Plus** icon and select **Block element**.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
3. Hover your mouse over the element you want to block.
<br>The element area turns yellow.
4. Double-click the element.
<br>The *Block element* window opens.
5. Click **Add**.
<br>The element is now blocked.
</section>

[Learn how to view a blocked item or unblock an element](adblockplus/view-blocked-items)

<section class="platform-opera" markdown="1">
## Opera

1. Open the webpage with the element you want to block.
2. From the Opera toolbar, click the **Adblock Plus** icon and select **Block element**.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
3. Hover your mouse over the element you want to block.
<br>The element area turns yellow.
4. Double-click the element.
<br>The *Block element* window opens.
5. Click **Add**.
<br>The element is now blocked.
</section>

[Learn how to view a blocked item or unblock an element](adblockplus/view-blocked-items)

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. Open the webpage with the element you want to block.
2. From the Yandex Browser toolbar, click the **Adblock Plus** icon and select **Block element**.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
3. Hover your mouse over the element you want to block.
<br>The element area turns yellow.
4. Double-click the element.
<br>The *Block element* window opens.
5. Click **Add**.
<br>The element is now blocked.
</section>

[Learn how to view a blocked item or unblock an element](adblockplus/view-blocked-items)
